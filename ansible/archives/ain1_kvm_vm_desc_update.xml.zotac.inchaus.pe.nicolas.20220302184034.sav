---
# Example:
# ansible-playbook ansible/ain1_kvm_vm_desc_update.xml -i MYHYPERVISORIP,
- name: "[   Libvirt/KVM Virtual Machine Creation                         ]"
  hosts:
    all
  gather_facts: no
  vars:
    ansible_python_interpreter: "/usr/bin/python3"
    timecode:                   "{{ lookup('pipe','date +%Y%m%d%H') }}"
    my_vm_libvirt_uri:          "qemu:///system"
  tasks:

###################   VM Update                      ###################

    - name: "[   Get VM list                                                  ]"
      shell: |
        LC_ALL=C virsh \
          -c {{ my_vm_libvirt_uri }} \
          list \
          --state-running \
          --name
      register: my_vm_list
      run_once: yes


    - name: "[   Get VM(s) Network Details                                    ]"
      shell: |
        LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-network-get-interfaces"}'
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_netif_list

    - name: "[   Get VM(s) OS Details                                         ]"
      shell: |
        LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-get-osinfo"}'
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_osinfo_list



    - name: "[   Debug                                                        ]"
      debug:
        msg: |
          {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1.name }}
          {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['hardware-address'] | default('unknown') }}
          {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] }}
          {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0.prefix }}
          #
          {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['kernel-release'] }}
          {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['pretty-name'] }}
          #
          #
          {{ ((( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='hardware-address')) | join(', ') }}
          {{ ((( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='ip-addresses')) | flatten | map(attribute='ip-address') | join(', ') }}
          {{ ((( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='ip-addresses')) | flatten | map(attribute='prefix') | join(', ') }}
          {{ ((( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='ip-addresses')) | flatten | map(attribute='ip-address') | reject('match', '127.0.0.1') | reject('match', '::1') | join('') }}
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
#      tags: ['never']

#          {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map('combine', 'ip-addresses') }}

    - name: "[   Get VM(s) Main User                                          ]"
      shell: |
        LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"cat", "arg": [ "/etc/passwd" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | grep .*:.*:1000: | cut -d: -f1
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_user_list

    - name: "[   Get VM(s) CPU number                                         ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          dominfo {{ my_vm_name }} | grep ^CPU.s | tr -d " " | cut -d: -f2
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_cpu_list

    - name: "[   Get VM(s) Mem                                                ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          dominfo {{ my_vm_name }} | grep Max | tr -s " " | cut -d\  -f3
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_mem_list

    - name: "[   Get VM(s) Storage                                            ]"
      shell: |
        echo $(($(virsh -c "{{ my_vm_libvirt_uri }}" guestinfo {{ my_vm_name }}  | grep total-bytes | tr -d " " | cut -d: -f2 | tr "\n" "+" | sed "s/+$//")+0))
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_storage_list

    - name: "[   Get VM(s) IP                                                 ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domifaddr \
          --source agent \
          --domain {{ my_vm_name }} | grep {{ my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') }} | tr -s ' ' | cut -d\  -f5 | cut -d\/ -f1
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_ip_list
      tags: ['never']

    - name: "[   Get VM(s) MAC                                                ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domifaddr \
          --source agent \
          --interface {{ my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') }} \
          --domain {{ my_vm_name }} | grep {{ my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') }} | tr -s ' ' | cut -d\  -f3
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_mac_list
      tags: ['never']

    - name: "[   Get VM(s) NETMASK                                            ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domifaddr \
          --source agent \
          --interface {{ my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') }} \
          --domain {{ my_vm_name }} | grep {{ my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') }} | tr -s ' ' | cut -d\  -f5 | cut -d\/ -f2
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_mask_list
      tags: ['never']

    - name: "[   Get VM(s) Gateway                                            ]"
      shell: |
        LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"ip", "arg": [ "route", "show", "default" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | cut -d\  -f3
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_gateway_list

    - name: "[   Get VM(s) DNS                                                ]"
      shell: |
        LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{ "execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"cat", "arg": [ "/etc/resolv.conf" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | grep ^nameserver | head -n 1 | cut -d\  -f2
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_dns_list

    - name: "[   Get VM(s) Distribution                                       ]"
      shell: |
       LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-get-osinfo"}' --pretty | jq --raw-output '.return ["pretty-name"]' | tr -d "\n"
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_distrib_list
      tags: ['never']

    - name: "[   Get VM(s) Kernel                                             ]"
      shell: |
       LC_ALL=C virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-get-osinfo"}' --pretty | jq --raw-output '.return ["kernel-release"]' | tr -d "\n"
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_kernel_list
      tags: ['never']

    - name: "[   Get VM(s) UUID                                               ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domuuid \
          --domain {{ my_vm_name }} | tr -d "\n" | tr -d " "
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_uuid_list

    - name: "[   Get VM(s) Hostname                                           ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domhostname \
          --source agent \
          --domain {{ my_vm_name }} | tr -d "\n" | tr -d " "
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_hostname_list

    - name: "[   Debug                                                        ]"
      debug: 
        msg: "{{ my_vm_ip_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}"
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      tags: [ 'never' ]

    - name: "[   Update VM(s) description                                     ]"
      shell: |
        LC_ALL=C virsh \
          -c {{ my_vm_libvirt_uri }} \
          desc \
          --live \
          --config {{ my_vm_name }} "# Usefull infos:
        VM name:            {{ my_vm_name }}
        VM UUID:            {{ my_vm_uuid_list.results     | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        VM Hostname:        {{ my_vm_hostname_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        VM Distribution:    {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['pretty-name'] }}
        VM Kernel:          {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['kernel-release'] }}
        VM CPU count:       {{ my_vm_cpu_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        VM RAM:             {{ my_vm_mem_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }} Bytes
        VM Storage:         {{ my_vm_storage_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }} Bytes
        VM Main User:       {{ my_vm_user_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        VM Net interface:   {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1.name | default('unknown') }}
        VM MAC:             {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['hardware-address'] | default('unknown') }}
        VM IP / Netmask:    {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] }} / {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0.prefix }}
        VM DNS:             {{ my_vm_dns_list.results      | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        VM Network Gateway: {{ my_vm_gateway_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
        
        # Console access:
        virsh -c 'qemu+ssh://{{ inventory_hostname }}/system' console --force --domain {{ my_vm_name }}
        
        # Network interface infos:
        virsh -c 'qemu+ssh://{{ inventory_hostname }}/system' domifaddr --source agent --domain {{ my_vm_name }}
        
        # Ping:
        ping -c 5 {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] | default('unknown') }}
        
        # SSH access:
        ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=20 -l {{ my_vm_user_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }} {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] | default('unknown') }}
        
        # Send Ctrl-Alt-Del keybinding:
        virsh -c 'qemu+ssh://{{ inventory_hostname }}/system' send-key {{ my_vm_name }} --holdtime 1000 KEY_LEFTCTRL KEY_LEFTALT KEY_DELETE"
        exit 0
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"


    - name: "[   Update VM(s) title                                           ]"
      shell: |
        LC_ALL=C virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          desc \
          --live \
          --config \
          --title {{ my_vm_name }} \
          "{{ my_vm_name }}$(head -c $((20 - {{ my_vm_name | length }})) /dev/zero | tr '\0' ' '){{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] | default('unknown') }}/{{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0.prefix | default('unknown') }}$(head -c $((16 - {{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['ip-addresses'].0['ip-address'] | default('unknown') | length }})) /dev/zero | tr '\0' ' '){{ (( my_vm_netif_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return.1['hardware-address'] | default('unknown') }}"
        exit 0
      loop: "{{ my_vm_list.stdout_lines }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"


###################   The End                        ###################

