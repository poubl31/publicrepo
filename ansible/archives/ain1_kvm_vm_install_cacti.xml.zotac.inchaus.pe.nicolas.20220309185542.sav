---
# Example:
# ansible-playbook ansible/ain1_kvm_vm_install_cacti.xml -u root -i VM1IP,
- name: "[   Libvirt/KVM VM IP Config Update                  ]"
  hosts: all
  gather_facts: True
  vars:
    my_vm_domain: "example.com"
    my_vm_mariadb_root_passwd: "abc123"
    my_vm_mariadb_db_name: "cactidb"
    my_vm_mariadb_user_name: "cactiuser"
    my_vm_mariadb_user_password: "abc123"
    my_vm_ssl_cert_subj: "/C=FR/ST=OC/O={{ my_vm_domain }}, Inc./CN={{ my_vm_domain }}"
    my_vm_ssl_cert_ext: "subjectAltName=DNS:{{ my_vm_domain }}"
    # ansible_python_interpreter: "/usr/bin/python3"
    # timecode:                  "{{ lookup('pipe','date +%Y%m%d%H') }}"
  handlers:
    - name: "[   Disable HTTPD                                    ]"
      service:
        name: httpd
        state: stopped
        enabled: no
      listen: "stop httpd service"
    - name: "[   Restart NGINX                                    ]"
      service:
        name: nginx
        state: restarted
        enabled: yes
      listen: "restart nginx service"
    - name: "[   Restart MariaDB                                  ]"
      service:
        name: mariadb
        state: restarted
        enabled: yes
      listen: "restart mariadb service"
    - name: "[   Restart PHP-FPM                                  ]"
      service:
        name: php-fpm
        state: restarted
        enabled: yes
      listen: "restart php_fpm service"
    - name: "[   Restart SNMPD                                   ]"
      service:
        name: snmpd
        state: restarted
        enabled: yes
      listen: "restart snmpd service"
  tasks:
    - name: "[   Get VM facts                                     ]"
      setup: gather_subset='all'

    - name: "[   Debug Facts                                      ]"
      debug: msg="{{ ansible_facts }}"
      tags: ['never']

    #######################################################

    - name: "[   Redhat/Fedora/CentOS Block                       ]"
      block:
        - name: "[   Redhat: Clear YUM cache                          ]"
          command: yum clean all
        - name: "[   Redhat: Enable EPEL repo                         ]"
          package:
            name:
              - "epel-release"
            state: present
            update_cache: yes
        - name: "[   Redhat: Packages installation                    ]"
          yum:
            name:
              - "nginx"
              - "mariadb-server"
              - "cacti"
              - "openssl"
              - "php-fpm"
              - "php"
              - "php-mysqlnd"
              - "php-pear"
              - "php-common"
              - "php-gd"
              - "php-devel"
              - "php-mbstring"
              - "php-cli"
              - "php-snmp"
              - "net-snmp-utils"
              - "net-snmp-libs"
              - "rrdtool"
            state: present
            update_cache: yes
          notify:
            - "stop httpd service" 
            - "restart nginx service" 
            - "restart mariadb service" 
            - "restart php_fpm service" 
            - "restart snmpd service" 

        - name: "[   Redhat: Flush handlers                           ]"
          meta: flush_handlers

        - name: "[   Redhat: Installation Commands                    ]"
          shell: |
            # HTTPS configuration
            mkdir -p /etc/pki/nginx/private
            openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
              -keyout /etc/pki/nginx/private/server.key \
              -out /etc/pki/nginx/server.crt \
              -subj "{{ my_vm_ssl_cert_subj }}" \
              -addext "{{ my_vm_ssl_cert_ext }}" \
              -newkey rsa:2048
            openssl dhparam -out /etc/pki/nginx/dhparam.pem 2048
            sed -i "/^#    server/,/^#    }/"' s/^#//' /etc/nginx/nginx.conf
            # MariaDB/MySQL configuration
            mysql_secure_installation <<EOF
            
            y
            {{ my_vm_mariadb_root_passwd }}
            {{ my_vm_mariadb_root_passwd }}
            y
            y
            y
            y
            EOF
            mysql -uroot -p{{ my_vm_mariadb_root_passwd }} -e "CREATE DATABASE {{ my_vm_mariadb_db_name }} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
            mysql -uroot -p{{ my_vm_mariadb_root_passwd }} -e "CREATE USER {{ my_vm_mariadb_user_name }}@localhost IDENTIFIED BY '{{ my_vm_mariadb_user_password }}';"
            mysql -uroot -p{{ my_vm_mariadb_root_passwd }} -e "GRANT ALL PRIVILEGES ON {{ my_vm_mariadb_db_name }}.* TO {{ my_vm_mariadb_user_name }}@'localhost';"
            mysql -uroot -p{{ my_vm_mariadb_root_passwd }} -e "FLUSH PRIVILEGES;"
            mysql -uroot -p{{ my_vm_mariadb_root_passwd }} cactidb < /usr/share/doc/cacti/cacti.sql
            find /usr/share/cacti/ /var/lib/cacti/ /usr/share/doc/cacti/ /etc/cacti/ -user apache -exec chown nginx:nginx {} \;
            chown root:nginx /etc/cacti/db.php
            chown -R nginx:nginx /var/log/cacti/
            sed -i "s/\(.database_default  = '\).*$/\1cactidb';/" /etc/cacti/db.php
            sed -i "s/\(.database_password = '\).*$/\1abc123';/" /etc/cacti/db.php
          notify:
            - "restart nginx service" 
            - "restart mariadb service" 
      when:
        - ( ansible_facts.distribution == 'CentOS' or ansible_facts.distribution == 'Fedora' or ansible_facts.distribution == 'Redhat' )
        - ansible_facts.devices.vda is defined

    #######################################################

    - name: "[   Debian Block                                     ]"
      block:
        - name: "[   No specific operation                            ]"
          debug: msg="No specific operation for {{ ansible_facts.distribution }}"
          # tags: ['never']
      when:
        - ansible_facts.distribution == 'Debian'
        - ansible_facts.devices.vda is defined

    #######################################################

    - name: "[   Alpine Block                                     ]"
      block:
        - name: "[   No specific operation                            ]"
          debug: msg="No specific operation for {{ ansible_facts.distribution }}"
          # tags: ['never']
      when:
        - ansible_facts.distribution == 'Alpine'
        - ansible_facts.devices.vda is defined


    #######################################################

