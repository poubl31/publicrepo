---
# Example:
# ansible-playbook ansible/ain1_kvm_vm_desc_update.xml -i MYHYPERVISORIP,
- name: "[   Libvirt/KVM VM Description Update        ]"
  hosts:
    all
  gather_facts: no
  vars:
    ansible_python_interpreter: "/usr/bin/python3"
    timecode:                   "{{ lookup('pipe','date +%Y%m%d%H') }}"
    my_vm_libvirt_uri:          "qemu:///system"
  tasks:

###################   VM Update                      ###################

    - name: "[   Get VM list                              ]"
      virt:
        command: list_vms
        state: running
      register: my_vm_list

    - name: "[   Get VM(s) XML configuration              ]"
      virt:
        command: get_xml
        name: "{{ my_vm_name }}"
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_xml_cfg_list

    - name: "[   Import XML - Machine                     ]"
      community.general.xml:
        xmlstring: "{{ ( my_vm_xml_cfg_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.get_xml }}"
        xpath: /domain/os/type
        content: attribute
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_xml_cfg_machine_list

    - name: "[   Import XML - CPU Mode                    ]"
      community.general.xml:
        xmlstring: "{{ ( my_vm_xml_cfg_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.get_xml }}"
        xpath: /domain/cpu
        content: attribute
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_xml_cfg_cpu_mode_list

    - name: "[   Debug                                    ]"
      debug:
        msg: |
          {{ my_vm_xml_cfg_all_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) }}
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      tags: ['never']

    - name: "[   Get VM(s) Guest Network Details          ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          qemu-agent-command \
          {{ my_vm_name }} \
          '{"execute": "guest-network-get-interfaces"}'
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_net_if_list

    - name: "[   Get VM(s) Virtual Network Bridge         ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domiflist \
          {{ my_vm_name }} | \
          grep "{{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | selectattr('hardware-address', 'defined') | rejectattr('hardware-address', 'match', '00:00:00:00:00:00') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | flatten | join(', ') }}" | \
          tr -s " " | cut -d " " -f4
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_net_bridge_list

    - name: "[   Get VM(s) Virtual Hypervisor interface   ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domiflist \
          {{ my_vm_name }} | \
          grep "{{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | selectattr('hardware-address', 'defined') | rejectattr('hardware-address', 'match', '00:00:00:00:00:00') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | flatten | join(', ') }}" | \
          tr -s " " | cut -d " " -f2
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_net_host_if_list

    - name: "[   Get VM(s) OS Details                     ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          qemu-agent-command \
          {{ my_vm_name }} \
          '{"execute": "guest-get-osinfo"}'
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_osinfo_list


    - name: "[   Get VM(s) CPU number                     ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          dominfo \
          {{ my_vm_name }} \
          | grep ^CPU.s | tr -d " " | cut -d: -f2
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_cpu_list

    - name: "[   Get VM(s) Mem                            ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          dominfo \
          {{ my_vm_name }} \
          | grep Max | tr -s " " | cut -d\  -f3
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_mem_list

    - name: "[   Get VM(s) Storage                        ]"
      shell: |
        echo $(($(virsh -c "{{ my_vm_libvirt_uri }}" guestinfo {{ my_vm_name }}  | grep total-bytes | tr -d " " | cut -d: -f2 | tr "\n" "+" | sed "s/+$//")+0))
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_storage_list

    - name: "[   Get VM(s) UUID                           ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domuuid \
          {{ my_vm_name }} \
          | tr -d "\n" | tr -d " "
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_uuid_list

    - name: "[   Get VM(s) Hostname                       ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          domhostname \
          --source agent \
          {{ my_vm_name }} \
          | tr -d "\n" | tr -d " "
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_hostname_list

    - name: "[   Get VM(s) Main User                      ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          qemu-agent-command \
          {{ my_vm_name }} \
          '{"execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"getent", "arg": [ "passwd", "1000" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | grep .*:.*:1000: | cut -d: -f1
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_user_list

    - name: "[   Get VM(s) Gateway                        ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          qemu-agent-command \
          {{ my_vm_name }} \
          '{"execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"ip", "arg": [ "route", "show", "default" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | cut -d\  -f3
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_gateway_list

    - name: "[   Get VM(s) DNS                            ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          qemu-agent-command \
          {{ my_vm_name }} \
          '{ "execute": "guest-exec-status", "arguments": { "pid": '$(virsh -c "{{ my_vm_libvirt_uri }}" qemu-agent-command {{ my_vm_name }} '{"execute": "guest-exec", "arguments": { "path":"cat", "arg": [ "/etc/resolv.conf" ], "capture-output": true } }' | jq ".return.pid")' } }' | jq --raw-output '.return | .["out-data"]' | base64 -d | grep ^nameserver | head -n 1 | cut -d\  -f2
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      register: my_vm_dns_list



    - name: "[   Debug                                    ]"
      debug:
        msg: |
          {{ my_vm_ip_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}
          {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['kernel-release'] }}
          {{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['pretty-name'] }}
          VM Main User:       {{ my_vm_user_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}
          VM Net interface:   {{ (( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'Loopback Pseudo-Interface 1') | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | rejectattr('name', 'equalto', 'lo') | map(attribute='name') | join('') }}
          MAC: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | selectattr('hardware-address', 'defined') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | join(', ') }}
          ALL IP: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='ip-addresses')) | flatten | map(attribute='ip-address') | join(', ') }}
          ALL MASK: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | map(attribute='ip-addresses')) | flatten | map(attribute='prefix') | join(', ') }}
          MAC, loopback filtered: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | selectattr('hardware-address', 'defined') | rejectattr('hardware-address', 'match', '00:00:00:00:00:00') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | flatten | join(', ') }}
          IP, loopback filtered: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | join(', ') }}
          Prefix, loopback filtered: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='prefix') | join(', ') }}
          IP/MASK, loopback filtered: {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | union(((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='prefix') ) | join('/') }}
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"
      tags: ['never']






    - name: "[   Update VM(s) description                 ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c {{ my_vm_libvirt_uri }} \
          desc \
          --live \
          --config {{ my_vm_name }} "# Usefull infos:
        CURRENT_VM_NAME=\"{{ my_vm_name | default('UNKNOWN', true) }}\"
        CURRENT_VM_UUID=\"{{ my_vm_uuid_list.results     | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') | default('UNKNOWN', true) }}\"
        CURRENT_VM_ARCH=\"{{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.arch }}\"
        CURRENT_VM_MACHINE=\"{{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.machine }}\"
        CURRENT_VM_CPU_MODE=\"{{ ( my_vm_xml_cfg_cpu_mode_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.cpu.mode }}\"
        CURRENT_VM_HOSTNAME=\"{{ my_vm_hostname_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') | default('UNKNOWN', true) }}\"
        CURRENT_VM_OS=\"{{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['pretty-name'] | default('UNKNOWN', true) }}\"
        CURRENT_VM_KERNEL=\"{{ (( my_vm_osinfo_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return['kernel-release'] | default('UNKNOWN', true) }}\"
        CURRENT_VM_CPU_COUNT=\"{{ my_vm_cpu_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') | default('UNKNOWN', true) }}\"
        CURRENT_VM_MEM=\"{{ my_vm_mem_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') | default('UNKNOWN', true) }}\"   # Bytes
        CURRENT_VM_HDD=\"{{ my_vm_storage_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join(' ') }}\"   # Bytes
        CURRENT_VM_USER=\"{{ my_vm_user_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}\"
        CURRENT_VM_BRIDGE=\"{{ my_vm_net_bridge_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}\"
        CURRENT_VM_HOST_INTF=\"{{ my_vm_net_host_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}\"
        CURRENT_VM_GUEST_INTF=\"{{ (( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'Loopback Pseudo-Interface 1') | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | rejectattr('name', 'equalto', 'lo') | map(attribute='name') | join('') }}\"
        CURRENT_VM_MAC=\"{{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | selectattr('hardware-address', 'defined') | rejectattr('hardware-address', 'match', '00:00:00:00:00:00') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | flatten | join(', ') }}\"
        CURRENT_VM_IP_MASK=\"{{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | union(((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='prefix') ) | join('/') }}\"
        CURRENT_VM_DNS=\"{{ my_vm_dns_list.results      | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}\"
        CURRENT_VM_GW=\"{{ my_vm_gateway_list.results  | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }}\"
        
        # Console access:
        virsh -c 'qemu+ssh://{{ inventory_hostname }}/system' console --force --domain {{ my_vm_name }}
        virt-viewer -c 'qemu+ssh://{{ inventory_hostname }}/system' -r -a {{ my_vm_name }}
        
        # Ping:
        ping -c 5 {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | join(', ') }}
        
        # SSH access:
        ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=20 -l {{ my_vm_user_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) | map(attribute='stdout') | join('') | default('UNKNOWN', true) }} {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | join(', ') }}
        
        # Send Ctrl-Alt-Del keybinding:
        virsh -c 'qemu+ssh://{{ inventory_hostname }}/system' send-key {{ my_vm_name }} --holdtime 1000 KEY_LEFTCTRL KEY_LEFTALT KEY_DELETE"
        exit 0
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"



    - name: "[   Update VM(s) title                       ]"
      shell: |
        LC_ALL=C.UTF-8 virsh \
          -c "{{ my_vm_libvirt_uri }}" \
          desc \
          --live \
          --config \
          --title {{ my_vm_name }} \
          "{{ my_vm_name }}$(head -c $((34 - {{ my_vm_name | length }} - {{ (((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | union(((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='prefix') ) | join('/') ) | length }} )) /dev/zero | tr '\0' ' '){{ (((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='ip-address') | union(((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | map(attribute='ip-addresses')) | flatten | selectattr('ip-address-type', 'equalto', 'ipv4') | rejectattr('ip-address', 'match', '127.0.0.1') | rejectattr('ip-address', 'match', '::1') | map(attribute='prefix') ) | join('/') ) }}  {{ ((( my_vm_net_if_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.stdout | from_json() ).return | rejectattr('name', 'equalto', 'vEthernet (Default Switch)') | selectattr('hardware-address', 'defined') | rejectattr('hardware-address', 'match', '00:00:00:00:00:00') | map(attribute='hardware-address') | default('UNKNOWN', true) ) | flatten | join(', ') }}  {{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.arch }}/{{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.machine }}$(head -c $((37 - {{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.arch | length }} - {{ ( my_vm_xml_cfg_machine_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.type.machine | length }} - {{ ( my_vm_xml_cfg_cpu_mode_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.cpu.mode | length }} )) /dev/zero | tr '\0' ' '){{ ( my_vm_xml_cfg_cpu_mode_list.results | selectattr('my_vm_name', 'equalto', my_vm_name) ).0.matches.0.cpu.mode }}"
        exit 0
      loop: "{{ my_vm_list.list_vms }}"
      loop_control:
        loop_var: my_vm_name
        label: "{{ my_vm_name }}"


###################   The End                        ###################

