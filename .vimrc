set history=500

" Enable filetype plugins
"filetype plugin on
"filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" :W sudo saves the file 
"command W w !sudo tee % > /dev/null

" Avoid garbled characters in Chinese language windows OS
"let $LANG='fr' 
set langmenu=fr

" Turn on the Wild menu
set wildmenu

set paste
set nonumber

"syntax off

