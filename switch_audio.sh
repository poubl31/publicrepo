#!/bin/bash

if test $(LC_ALL=C pactl list | grep -c -i active.profile..output.analog-stereo) == 1 ; then
  pactl set-card-profile $(pactl list cards short | cut -f1) output:hdmi-stereo
else
  pactl set-card-profile $(pactl list cards short | cut -f1) output:analog-stereo
fi

