#!/bin/bash

if [ ${WAYLAND_DISPLAY} ] ; then
  wl-paste | xargs mpv
else
  xclip -o | xargs mpv
fi
