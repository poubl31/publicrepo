#!/bin/bash

if [ -f /etc/debian_version ] ; then
 sudo apt install \
   python3-pip git
elif [ -f /etc/alpine-release ] ; then
 sudo apk install \
   python3-pip git
elif [ -f /etc/redhat-release ] ; then
 sudo dnf install \
   python3-pip git
fi

pip install         \
  jinja2            \
  PyYAML            \
  six               \
  cryptography      \
  wheel             \
  setuptools        \
  requests          \
  netaddr           \
  netmiko           \
  paramiko          \
  ansible           \
  ansible-tower-cli \
  influxdb          \
  libvirt-python

ansible-galaxy collection install \
  ansible.netcommon \
  ansible.utils \
  community.postgresql \
  community.mysql \
  community.libvirt \
  community.general \
  cisco.aci \
  cisco.nxos \
  cisco.ios \
  cisco.iosxr \
  cisco.nd \
  cisco.mso \
  junipernetworks.junos \
  infoblox.nios_modules \
  axiansdeveloper.ipfabric \
  servicenow.servicenow
