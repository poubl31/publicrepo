#!/bin/bash

# How to use it:
# bash -c 'source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/get_files.sh)'


if [ "$1" == "admin" ] ; then
  [ -f /etc/debian_version ] && sudo apt install wget curl bash-completion tmux
  [ -f /etc/alpine-release ] && sudo apk install wget curl bash-completion tmux
  [ -f /etc/redhat-release ] && sudo dnf install wget curl bash-completion tmux
  if [ ! -f /etc/bash.mine     ]; then sudo wget -O /etc/bash.mine https://bitbucket.org/poubl31/publicrepo/raw/HEAD/bash.mine  ; fi
fi

if [ ! -f ${HOME}/.inputrc   ]; then wget -O ${HOME}/.inputrc     https://bitbucket.org/poubl31/publicrepo/raw/HEAD/.inputrc   ; fi
if [ ! -f ${HOME}/.screenrc  ]; then wget -O ${HOME}/.screenrc    https://bitbucket.org/poubl31/publicrepo/raw/HEAD/.screenrc  ; fi
if [ ! -f ${HOME}/.tmux.conf ]; then wget -O ${HOME}/.tmux.conf   https://bitbucket.org/poubl31/publicrepo/raw/HEAD/.tmux.conf ; fi
if [ ! -f ${HOME}/.vimrc     ]; then wget -O ${HOME}/.vimrc       https://bitbucket.org/poubl31/publicrepo/raw/HEAD/.vimrc     ; fi
if [ ! -f ${HOME}/.bash.mine ]; then wget -O ${HOME}/.bash.mine   https://bitbucket.org/poubl31/publicrepo/raw/HEAD/bash.mine  ; fi

if   [ -f ${HOME}/.profile ]      ; then my_bash_conf_file="${HOME}/.profile"
elif [ -f ${HOME}/.bash_profile ] ; then my_bash_conf_file="${HOME}/.bash_profile"
elif [ -f ${HOME}/.bash_login ] ; then my_bash_conf_file="${HOME}/.bash_login"
else my_bash_conf_file="${HOME}/.bash_login" ; touch ${HOME}/.bash_login
fi

if [ -f ${HOME}/.bash.mine ] && [ `grep -c MYOWNSCRIPTSBEGIN ${my_bash_conf_file}` == 0 ] ; then
  /bin/echo  -e "
# MYOWNSCRIPTSBEGIN
if [ -n \${BASH_VERSION} ] && [ -f ${HOME}/.bash.mine ]; then
  . ${HOME}/.bash.mine
fi
# MYOWNSCRIPTSEND
" >> ${my_bash_conf_file}
fi


if [ "${1}" == "clean" ] ; then
  sudo /bin/rm -v -f  \
    ${HOME}/.inputrc \
    ${HOME}/.screenrc \
    ${HOME}/.tmux.conf \
    ${HOME}/.vimrc \
    /etc/bash.mine
fi

unset my_bash_conf_file

exit 0

