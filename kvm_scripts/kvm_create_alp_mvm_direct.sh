#!/bin/bash

# How to use it:
# bash -c 'my_vm_name="mytestvm2" ; virsh destroy ${my_vm_name} ; virsh undefine ${my_vm_name} --nvram --remove-all-storage'
# bash -c 'my_vm_name="mytestvm2" my_vm_username="admin2" my_vm_network="brkvm" source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_create_alpine_mvm_direct.sh)'

LC_ALL=C

my_vm_name="${my_vm_name:-mytestvm}"
my_vm_username="${my_vm_username:-admin}"

my_vm_password="${my_vm_password:-LRbdkRcQl5FFc}"   # usermod password : openssl passwd or mkpasswd -m sha-512 PASSWORD (whois package)
my_vm_ssh_pub_keys=`for i in ${HOME}/.ssh/*pub ; do cat "${i}" ; /bin/echo -e "\n" | grep -v ^$  ; done`
my_vm_domain="${my_vm_domain:-$(hostname -d)}"
my_vm_net_iface="${my_vm_net_iface:-eth0}"
my_vm_network="${my_vm_network:-brkvm}"
my_vm_net_dns="${my_vm_net_dns:-8.8.8.8}"
my_vm_cpu_nbr="${my_vm_cpu_nbr:-2}"
my_vm_mem_size="${my_vm_mem_size:-4096}"
my_vm_machine="${my_vm_machine:-microvm}"
my_vm_cpu_model="${my_vm_cpu_model:-host-passthrough}"
my_vm_disk_size="${my_vm_disk_size:-20}"
my_vm_alp_os_version="${my_vm_alp_os_version:-3.8}"
my_vm_alp_real_os_version="${my_vm_alp_real_os_version:-3.15}"
my_vm_default_pool_dir="$(virsh pool-dumpxml default | grep path | cut -d\> -f2 | cut -d\< -f1)"
my_vm_alp_script_url="${my_vm_alp_script_url:-https://raw.githubusercontent.com/alpinelinux/alpine-make-vm-image/master/alpine-make-vm-image}"
my_vm_alp_script_file="${my_vm_alp_script_file:-/tmp/alpine-make-vm-image}"
my_vm_alp_cfg_file="alpine-cfg-init.sh"
my_vm_packages_to_install="openssh-server qemu-guest-agent e2fsprogs screen tmux sudo bash bash-completion shadow htop curl wget"

my_vm_kernel_file="bzImage"
my_vm_kernel_args="root=/dev/vda ro nosplash text selinux=0 apparmor=0 ipv6.disable=0 biosdevname=1 net.ifnames=1 console=ttyS0,115200 earlyprintk=ttyS0,115200 consoleblank=0 systemd.show_status=true reboot=k ip=::::${my_vm_name}::on:::"

my_vm_git_repo_path="https://bitbucket.org/poubl31/publicrepo/raw/HEAD/"

my_vm_get_files_script_path="${my_vm_git_repo_path}get_files.sh"

my_vm_sysctl_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_add_sysctl_files.sh"
my_vm_sysctl_script_name="/tmp/`basename ${my_vm_sysctl_script_path}`"

my_vm_sudo_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_add_sudo_files.sh"
my_vm_sudo_script_name="/tmp/`basename ${my_vm_sudo_script_path}`"

my_vm_customize_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_customize_vm.sh"
my_vm_customize_script_name="/tmp/`basename ${my_vm_customize_script_path}`"

my_vm_quiet_option="-q"

#my_vm_name=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1)
#my_vm_username="${USER:-${USERNAME}}"
#my_vm_net_dns="$(virsh net-dumpxml ${my_vm_network} | grep "ip address" | cut -d\' -f2)"



if [ ! -e ${my_vm_alp_script_file} ] ; then
  wget -q -O ${my_vm_alp_script_file} ${my_vm_alp_script_url}
  chmod +x ${my_vm_alp_script_file}
fi

if [ ! -d /tmp/${my_vm_name} ] ; then
  mkdir -p /tmp/${my_vm_name}/scripts/
fi

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Kernel check"
if [ ! -f ${my_vm_default_pool_dir}/${my_vm_kernel_file} ] ; then
  if [ $(curl --silent -I ${my_vm_git_repo_path}kvm_scripts/${my_vm_kernel_file} | grep -E "^HTTP" | awk -F " " '{print $2}') == 404 ] ; then
    echo "${my_vm_git_repo_path}kvm_scripts/${my_vm_kernel_file} doesn't exist"
    exit 1
  fi
  wget \
    -c \
    -P ${my_vm_default_pool_dir} \
    ${my_vm_git_repo_path}kvm_scripts/${my_vm_kernel_file}
fi

##############################################################################


cat > /tmp/${my_vm_name}/scripts/${my_vm_alp_cfg_file} <<-EOFINITSCRIPT
#!/bin/sh

echo 'Set up networking'
cat > /etc/network/interfaces <<-EOFINTF
iface lo inet loopback
iface ${my_vm_net_iface} inet dhcp
 hostname ${my_vm_name}
EOFINTF
ln -s networking /etc/init.d/net.lo
rc-update add net.lo boot
#ln -s networking /etc/init.d/net.${my_vm_net_iface}
#rc-update add net.${my_vm_net_iface} default

echo 'Set up Timezone'
setup-timezone -z Europe/Paris

echo 'Set up Hostname'
setup-hostname ${my_vm_name}

echo 'Set up Keymap'
setup-keymap fr fr

echo 'Set up DNS'
setup-dns -d ${my_vm_domain} -n ${my_vm_net_dns}

echo 'Adjust rc.conf'
sed -Ei \
 -e 's/^[# ](rc_depend_strict)=.*/\1=NO/' \
 -e 's/^[# ](rc_logger)=.*/\1=YES/' \
 -e 's/^[# ](unicode)=.*/\1=YES/' \
 /etc/rc.conf

echo 'Enable Users'
chsh -s /bin/bash root
useradd -m -d /home/${my_vm_username} -g 100 -G wheel -s /bin/bash ${my_vm_username}
usermod -p "${my_vm_password}" ${my_vm_username}
usermod -p "${my_vm_password}" root
#echo "${my_vm_username} ALL=(ALL) ALL" > /etc/sudoers.d/${my_vm_username}

echo 'SSH keys'
mkdir /root/.ssh
cat > /root/.ssh/authorized_keys <<-EOFSSHKEY
${my_vm_ssh_pub_keys}
EOFSSHKEY
mkdir /home/${my_vm_username}/.ssh
cp /root/.ssh/authorized_keys /home/${my_vm_username}/.ssh/ 
chown -R ${my_vm_username}:users /home/${my_vm_username}/.ssh

echo 'Sysctl configuration'
wget -O ${my_vm_sysctl_script_name} ${my_vm_sysctl_script_path}
chmod +x ${my_vm_sysctl_script_name}
my_vm_name=${my_vm_name} my_vm_domain=${my_vm_domain} my_vm_net_iface=${my_vm_net_iface} ${my_vm_sysctl_script_name}

echo 'Sudo configuration'
wget -O ${my_vm_sudo_script_name} ${my_vm_sudo_script_path}
chmod +x ${my_vm_sudo_script_name}
my_vm_username=${my_vm_username} ${my_vm_sudo_script_name}

echo 'Import remote files'
su ${my_vm_username} -c 'bash <(curl -s ${my_vm_get_files_script_path})'

echo 'Update inittab file'
sed -i 's/^\(tty[2-6]\)/#\1/' /etc/inittab

echo 'Set up Bootloader'
cat > /boot/extlinux.conf <<-EOFSYSLINUX
SERIAL ttyS0 115200
DEFAULT menu.c32
PROMPT 0
MENU TITLE Alpine/Linux Boot Menu
MENU HIDDEN
MENU AUTOBOOT Alpine will be booted automatically in # seconds.
TIMEOUT 1
LABEL virt
MENU LABEL Linux virt
LINUX vmlinuz-virt
  INITRD initramfs-virt
  APPEND root=/dev/vda modules=ext4 console=ttyS0 time reboot=k ip=::::${my_vm_name}::on:::
  MENU SEPARATOR
EOFSYSLINUX

echo 'SSH service activation'
sed -i 's/^#\{0,1\}\(AddressFamily \).*/\1inet/' /etc/ssh/sshd_config
rc-update add sshd

echo 'Qemu Guest Agent service activation'
#echo 'GA_PATH="/dev/vport1p1"' >> /etc/conf.d/qemu-guest-agent
rc-update add qemu-guest-agent

echo 'NTP service activation'
sed -i "s/\(^NTPD_OPTS=.*$\)/#\1/" /etc/conf.d/ntpd
echo "NTPD_OPTS=\"-N -p 80.74.64.1 -p 195.83.132.135\"" >> /etc/conf.d/ntpd
rc-update add ntpd

EOFINITSCRIPT

##############################################################################

chmod +x  /tmp/${my_vm_name}/scripts/${my_vm_alp_cfg_file}

sudo ${my_vm_alp_script_file} \
  -f qcow2 \
  -b v${my_vm_alp_real_os_version} \
  -s ${my_vm_disk_size}G \
  -p "${my_vm_packages_to_install}" \
  -c \
  -t ${my_vm_default_pool_dir}/${my_vm_name}.qcow2 \
  /tmp/${my_vm_name}/scripts/${my_vm_alp_cfg_file}
sudo chown ${USER:-${USERNAME}}:users ${my_vm_default_pool_dir}/${my_vm_name}.qcow2

virt-install \
  ${my_vm_quiet_option} \
  --autoconsole none \
  --name ${my_vm_name} \
  --os-type=linux \
  --os-variant alpinelinux${my_vm_alp_os_version} \
  --machine ${my_vm_machine}  \
  --memory ${my_vm_mem_size} \
  --cpu ${my_vm_cpu_model} \
  --graphics none \
  --vcpus ${my_vm_cpu_nbr} \
  --clock offset=localtime,rtc_tickpolicy=catchup \
  --controller type=usb,model=none \
  --controller type=pci,model=pci-root,driver.iommu=on \
  --controller type=virtio-serial,model=virtio,driver.iommu=on,address.type=virtio-mmio \
  --channel unix,mode=bind,path=/var/lib/libvirt/qemu/${my_vm_name}.agent,target_type=virtio,name=org.qemu.guest_agent.0 \
  --memballoon driver.iommu=on,address.type=virtio-mmio \
  --rng builtin,driver.iommu=on,address.type=virtio-mmio \
  --network network=${my_vm_network},model=virtio,driver.iommu=on,address.type=virtio-mmio \
  --disk $(virsh -q pool-refresh default ; virsh vol-path ${my_vm_name}.qcow2 --pool default),format=qcow2,bus=virtio,driver.iommu=on,address.type=virtio-mmio \
  --boot kernel=${my_vm_default_pool_dir}/${my_vm_kernel_file},kernel_args="${my_vm_kernel_args}"


#  --clock offset=localtime,rtc_tickpolicy=catchup \
#  --connect qemu:///system \
#  --machine q35 \

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Waiting for network"
while [[ ! $(virsh domifaddr --domain ${my_vm_name} --source agent --interface ${my_vm_net_iface} 2>/dev/null | grep ${my_vm_net_iface} | tr -s \  | cut -d\  -f5 | cut -d\/ -f1 | tr -d '\n') =~ [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} ]] ; do
  sleep 1
done

sleep 2

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Console and SSH connection commands"
echo virsh dominfo ${my_vm_name}
echo virsh domifaddr ${my_vm_name} --source agent
echo virsh console ${my_vm_name}
echo ssh \
  -o StrictHostKeyChecking=no \
  -o UserKnownHostsFile=/dev/null \
  -o ConnectTimeout=20 \
  -l ${my_vm_username} \
  $(virsh domifaddr --domain ${my_vm_name} --source agent --interface ${my_vm_net_iface} | grep ${my_vm_net_iface} | tr -s " " | cut -d\  -f5 | cut -d\/ -f1)



if [ -d /tmp/${my_vm_name} ] ; then
  /bin/rm -rf /tmp/${my_vm_name}
fi


unset my_vm_name my_vm_username my_vm_password my_vm_ssh_pub_keys my_vm_domain my_vm_net_iface my_vm_network my_vm_net_dns my_vm_cpu_nbr my_vm_mem_size my_vm_machine my_vm_cpu_model my_vm_disk_size my_vm_alp_os_version my_vm_alp_real_os_version my_vm_default_pool_dir my_vm_alp_script_url my_vm_alp_script_file my_vm_alp_cfg_file my_vm_packages_to_install my_vm_kernel_file my_vm_kernel_args my_vm_get_files_script_path my_vm_sysctl_script_path my_vm_sysctl_script_name my_vm_sudo_script_path my_vm_sudo_script_name my_vm_customize_script_path my_vm_customize_script_name my_vm_quiet_option my_vm_name my_vm_username

exit 0

