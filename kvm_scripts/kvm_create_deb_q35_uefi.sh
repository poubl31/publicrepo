#!/bin/bash -x

# How to use it:
# bash -c 'my_vm_name="mytestvm2" ; virsh destroy ${my_vm_name} ; virsh undefine ${my_vm_name} --nvram --remove-all-storage'
# bash -c 'my_vm_name="mytestvm2" my_vm_username="admin2" my_vm_password="abc123" my_vm_domain="example.com" my_vm_network="brkvm" my_vm_net_iface="enp1s0" my_vm_net_mtu=1370 source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_create_deb_q35_uefi.sh)'

LC_ALL=C

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Variables init"
my_vm_name="${my_vm_name:-mytestvm}"
my_vm_domain="${my_vm_domain:-$(hostname -d)}"
my_vm_machine="q35"
my_vm_os_variant="${my_vm_os_variant:-debian11}"
my_vm_username="${my_vm_username:-admin}"
my_vm_password="${my_vm_password:-abc123}"
my_vm_net_iface="${my_vm_net_iface:-enp1s0}"
my_vm_net_mtu=${my_vm_net_mtu:-1370}
my_vm_last_distrib_img_url_path="${my_vm_last_distrib_img_url_path:-https://cdimage.debian.org/cdimage/cloud/bullseye/latest/}"
my_vm_last_distrib_img_url_file="${my_vm_last_distrib_img_url_file:-debian-11-genericcloud-amd64.qcow2}"
my_vm_network="${my_vm_network:-brkvm}"
my_vm_disk_size="${my_vm_disk_size:-20G}"
my_vm_disk_format="qcow2"
my_vm_default_pool_dir="$(virsh pool-dumpxml default | grep path | cut -d\> -f2 | cut -d\< -f1)"
my_vm_mem_size="${my_vm_mem_size:-4096}"
my_vm_cpu_model="${my_vm_cpu_model:-host-passthrough}"
my_vm_cpu_nbr="${my_vm_cpu_nbr:-2}"
my_vm_packages_to_install="qemu-guest-agent,sudo,tmux,bash-completion,curl,wget,bind9-dnsutils"
my_vm_packages_to_uninstall="cloud-init"
my_vm_git_repo_path="https://bitbucket.org/poubl31/publicrepo/raw/HEAD/"
my_vm_get_files_script_path="${my_vm_git_repo_path}get_files.sh"
my_vm_sudo_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_add_sudo_files.sh"
my_vm_sudo_script_name="/tmp/`basename ${my_vm_sudo_script_path}`"
my_vm_sysctl_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_add_sysctl_files.sh"
my_vm_sysctl_script_name="/tmp/`basename ${my_vm_sysctl_script_path}`"
my_vm_customize_script_path="${my_vm_git_repo_path}kvm_scripts/kvm_customize_vm.sh"
my_vm_customize_script_name="/tmp/`basename ${my_vm_customize_script_path}`"
my_vm_quiet_option="-q"

SSH_INJECT=""
[ -f ${HOME}/.ssh/id_rsa.pub ] && SSH_INJECT="--ssh-inject ${my_vm_username}:file:${HOME}/.ssh/id_rsa.pub --ssh-inject root:file:${HOME}/.ssh/id_rsa.pub"

#####################

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Checks"
if test $(virsh domid ${my_vm_name} 2>/dev/null) ; then
  echo "VM ${my_vm_name} exists"
  exit 1
fi

for i in ${my_vm_default_pool_dir}/${my_vm_name}.${my_vm_disk_format} ; do
  if [ -f ${i} ] ; then
    echo "${i} exists"
    exit 1
  fi
done

if [ $(curl --silent -I ${my_vm_last_distrib_img_url_path}${my_vm_last_distrib_img_url_file} | grep -E "^HTTP" | awk -F " " '{print $2}') == 404 ] ; then
  echo "${my_vm_last_distrib_img_url_path}${my_vm_last_distrib_img_url_file} doesn't exist"
  exit 1
fi

#####################

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Original image download"
[[ -f ${my_vm_default_pool_dir}/${my_vm_last_distrib_img_url_file} ]] || \
wget \
  -P ${my_vm_default_pool_dir} \
  ${my_vm_last_distrib_img_url_path}${my_vm_last_distrib_img_url_file}

#####################

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Image creation"
cp ${my_vm_default_pool_dir}/${my_vm_last_distrib_img_url_file} ${my_vm_default_pool_dir}/${my_vm_name}.${my_vm_disk_format}
qemu-img resize \
  ${my_vm_quiet_option} \
  ${my_vm_default_pool_dir}/${my_vm_name}.${my_vm_disk_format} \
  +${my_vm_disk_size}

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Image customization"
virt-customize \
  ${my_vm_quiet_option} \
  -a ${my_vm_default_pool_dir}/${my_vm_name}.${my_vm_disk_format} \
  --hostname ${my_vm_name} \
  --uninstall ${my_vm_packages_to_uninstall} \
  --install ${my_vm_packages_to_install} \
  \
  --run-command "wget -O ${my_vm_customize_script_name} ${my_vm_customize_script_path} ; chmod +x ${my_vm_customize_script_name}" \
  --run-command "my_vm_name=${my_vm_name} my_vm_username=${my_vm_username} my_vm_net_iface=${my_vm_net_iface} my_vm_net_mtu=${my_vm_net_mtu} ${my_vm_customize_script_name}" \
  \
  --run-command "wget -O ${my_vm_sysctl_script_name} ${my_vm_sysctl_script_path} ; chmod +x ${my_vm_sysctl_script_name}" \
  --run-command "my_vm_name=${my_vm_name} my_vm_domain=${my_vm_domain} my_vm_net_iface=${my_vm_net_iface} ${my_vm_sysctl_script_name}" \
  \
  --run-command "wget -O ${my_vm_sudo_script_name} ${my_vm_sudo_script_path} ; chmod +x ${my_vm_sudo_script_name}" \
  --run-command "my_vm_username=${my_vm_username} ${my_vm_sudo_script_name}" \
  \
  --run-command "sed -i \"s/\(127.0.0.1.*$\)/\1 ${my_vm_name} ${my_vm_name}.${my_vm_domain}/\" /etc/hosts" \
  --run-command "su ${my_vm_username} -c 'bash <(curl -s ${my_vm_get_files_script_path})'" \
  \
  --root-password password:${my_vm_password} \
  --password ${my_vm_username}:password:${my_vm_password} \
  ${SSH_INJECT} \
  \
  --firstboot-command '[ -f /etc/debian_version ] && dpkg-reconfigure --frontend=noninteractive openssh-server' \
  --selinux-relabel


#####################

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : VM creation"
virt-install \
  ${my_vm_quiet_option} \
  --autoconsole none \
  --name ${my_vm_name} \
  --os-variant ${my_vm_os_variant} \
  --machine ${my_vm_machine}  \
  --memory ${my_vm_mem_size} \
  --cpu ${my_vm_cpu_model} \
  --graphics none \
  --sound none \
  --vcpus ${my_vm_cpu_nbr} \
  --features acpi=on,apic.eoi=on \
  --controller type=usb,model=none \
  --controller type=pci,model=pcie-root \
  --channel unix,mode=bind,name=org.qemu.guest_agent.0 \
  --memballoon model=virtio \
  --rng builtin \
  --clock offset=localtime,rtc_tickpolicy=catchup \
  --network network=${my_vm_network},model=virtio \
  --disk source.file=${my_vm_default_pool_dir}/${my_vm_name}.${my_vm_disk_format},target.bus=virtio \
  --boot uefi,hd,menu=off,useserial=on

#  --rng /dev/urandom \


echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Waiting for network"
while [[ ! $(virsh domifaddr --domain ${my_vm_name} --source agent --interface ${my_vm_net_iface} 2>/dev/null | grep ${my_vm_net_iface} | tr -s \  | cut -d\  -f5 | cut -d\/ -f1 | tr -d '\n') =~ [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} ]] ; do
  sleep 1
done

sleep 2

echo "$(/bin/date +%Y/%m/%d_%Hh%Mm%S) : Console and SSH connection commands"
echo virsh dominfo ${my_vm_name}
echo virsh domifaddr ${my_vm_name} --source agent
echo virsh console ${my_vm_name}
echo ssh \
  -o StrictHostKeyChecking=no \
  -o UserKnownHostsFile=/dev/null \
  -o ConnectTimeout=20 \
  -l ${my_vm_username} \
  $(virsh domifaddr --domain ${my_vm_name} --source agent --interface ${my_vm_net_iface} | grep ${my_vm_net_iface} | tr -s " " | cut -d\  -f5 | cut -d\/ -f1)

unset my_vm_name my_vm_domain my_vm_machine my_vm_os_variant my_vm_username my_vm_password my_vm_net_iface my_vm_net_mtu my_vm_last_distrib_img_url_path my_vm_last_distrib_img_url_file my_vm_network my_vm_disk_size my_vm_disk_format my_vm_default_pool_dir my_vm_mem_size my_vm_cpu_model my_vm_cpu_nbr my_vm_packages_to_install my_vm_packages_to_uninstall my_vm_get_files_script_path my_vm_sudo_script_path my_vm_sudo_script_name my_vm_sysctl_script_path my_vm_sysctl_script_name my_vm_customize_script_path my_vm_customize_script_name my_vm_quiet_option

exit 0

