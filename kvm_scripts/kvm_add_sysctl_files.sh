#!/bin/bash

# How to use it:
# bash -c 'my_vm_name=mytestvm2 my_vm_domain=example.com my_vm_net_iface=eth0 source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_add_sysctl_files.sh)'

my_vm_name="${my_vm_name:-mytestvm}"
my_vm_domain="${my_vm_domain:-$(hostname -d)}"
my_vm_net_iface="${my_vm_net_iface:-enp1s0}"

if [ ${my_vm_name} ] && [ ${my_vm_domain} ] && [ ${my_vm_net_iface} ] ; then
  if [ ! -f /etc/sysctl.d/10-${my_vm_name}.conf ] ; then
    my_vm_sysctl="kernel.domainname=${my_vm_domain}
kernel.hostname=${my_vm_name}
net.ipv4.ip_forward=0
net.ipv6.conf.all.disable_ipv6=0
net.ipv6.conf.default.disable_ipv6=1
net.ipv6.conf.lo.disable_ipv6=0
net.ipv6.conf.${my_vm_net_iface}.disable_ipv6=1"
    /bin/echo -e "${my_vm_sysctl}" > /etc/sysctl.d/10-${my_vm_name}.conf
  fi
fi

