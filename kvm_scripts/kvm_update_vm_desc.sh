#!/bin/bash

# How to use it:
# bash -c 'my_vm_username="admin2" my_vm_domain="example.com" my_vm_net_dns="8.8.8.8" source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_update_vm_desc.sh)'

export LC_ALL=C

##my_vm_username="${USER:-${USERNAME}}"
#my_vm_username="${1:-admin}"
#my_vm_domain="${2:-$(hostname -d)}"
#my_vm_net_dns="${3:-8.8.8.8}"

my_vm_username="${my_vm_username:-admin}"
my_vm_domain="${my_vm_domain:-$(hostname -d)}"
my_vm_net_dns="${my_vm_net_dns:-8.8.8.8}"

my_vm_quiet_option="-q"
verbosity=no

for current_vm in `virsh list --state-running --name` ; do
  my_vm_net_name="$(virsh dumpxml ${current_vm} | grep source\ netw | cut -d\' -f2)"
  my_vm_net_gw="$(ip route | grep $(virsh net-dumpxml brkvm | grep portgroup.*default.*yes | cut -d\' -f2) | grep link.src | cut -d\  -f9)"
  my_vm_net_mac="$(virsh dumpxml ${current_vm} | grep mac\ address | grep mac\ address | tr -s ' ' | cut -d\'  -f2)"
  my_vm_net_ip="" ; while [ "${my_vm_net_ip}" == "" ]; do my_vm_net_ip=$(ip neigh show  | grep ${my_vm_net_mac} | cut -d\  -f1) ; sleep 1 ; done
  my_vm_net_mask=$(ip route show | grep ^${my_vm_net_ip%???} | cut -d\  -f1 | cut -d\/ -f2)

  [ "${verbosity}" == "yes" ] && echo -e "# Show Previous Title:"
  [ "${verbosity}" == "yes" ] && virsh desc --title ${current_vm}

  [ "${verbosity}" == "yes" ] && echo -e "# Show Previous Description:"
  [ "${verbosity}" == "yes" ] && virsh desc ${current_vm}

  [ "${verbosity}" == "yes" ] && echo -e "# Set New Description:"
  virsh ${my_vm_quiet_option} desc --live --config ${current_vm} \
"Name    : ${current_vm}
Domain  : ${my_vm_domain}
User    : ${my_vm_username}
IP      : ${my_vm_net_ip}
MASK    : ${my_vm_net_mask}
VMMAC   : ${my_vm_net_mac}
GW      : ${my_vm_net_gw}
DNS     : ${my_vm_net_dns}
Network : ${my_vm_net_name}
VM UUID : $(virsh domuuid ${current_vm})

Console:
virsh console ${current_vm}

SSH:
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l ${my_vm_username} ${my_vm_net_ip}"



  [ "${verbosity}" == "yes" ] && echo -e "# Set New Title:"
  virsh -q desc --live --config --title ${current_vm} \
   "${current_vm}$(head -c $((21 - ${#current_vm})) /dev/zero | tr '\0' ' ')${my_vm_net_ip}/${my_vm_net_mask}$(head -c $((15 - ${#my_vm_net_ip})) /dev/zero | tr '\0' ' ')${my_vm_net_mac}"

  [ "${verbosity}" == "yes" ] && echo -e "# Show New Title:"
  [ "${verbosity}" == "yes" ] && virsh desc --title ${current_vm}

  [ "${verbosity}" == "yes" ] && echo -e "# Show New Description:"
  [ "${verbosity}" == "yes" ] && virsh desc ${current_vm}

done

exit 0


