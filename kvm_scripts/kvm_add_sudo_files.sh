#!/bin/bash

# How to use it:
# bash -c 'my_vm_username=admin2 source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_add_sudo_files.sh)'

my_vm_username="${my_vm_username:-admin}"

if [ ! -d /etc/sudoers.d ] ; then
  mkdir /etc/sudoers.d
fi

if [ ${my_vm_username} ] ; then
  if [ ! -f /etc/sudoers.d/020_sudo_perso_env ] ; then
    echo 'Defaults env_keep+="SSH_CLIENT SSH_CONNECTION SSH_TTY LANG ftp_proxy http_proxy https_proxy no_proxy"' > /etc/sudoers.d/020_sudo_perso_env
  fi

  if [ ! -f /etc/sudoers.d/020_sudo_perso_power ] ; then
    echo 'Cmnd_Alias POWER = /sbin/shutdown, /sbin/reboot, /sbin/poweroff, /sbin/halt, /usr/sbin/pm-*, /bin/systemctl reboot, /bin/systemctl poweroff*, /bin/systemctl suspend, /bin/systemctl hibernate' > /etc/sudoers.d/020_sudo_perso_power
  fi

  if [ ! -f /etc/sudoers.d/030_sudo_perso_${my_vm_username} ] ; then
    echo "${my_vm_username} ALL=(ALL) NOPASSWD: POWER" > /etc/sudoers.d/030_sudo_perso_${my_vm_username}
    echo "#${my_vm_username} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/030_sudo_perso_${my_vm_username}
  fi

  chmod 600 \
    /etc/sudoers.d/020_sudo_perso_env \
    /etc/sudoers.d/020_sudo_perso_power \
    /etc/sudoers.d/030_sudo_perso_${my_vm_username}
fi
