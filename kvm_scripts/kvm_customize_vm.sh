#!/bin/bash

# How to use it:
# bash -c 'my_vm_name=mytestvm2 my_vm_username=admin2 my_vm_net_iface=eth0 my_vm_net_mtu=1370 source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_customize_vm.sh)'

my_vm_name="${my_vm_name:-mytestvm}"
my_vm_username="${my_vm_username:-admin}"
my_vm_net_iface="${my_vm_net_iface:-eth0}"
my_vm_net_mtu="${my_vm_net_mtu:-1370}"

my_vm_domain="${my_vm_domain:-example.com}"
my_vm_net_dns="${my_vm_net_dns:-8.8.8.8}"

my_vm_ntp1="80.74.64.1"
my_vm_ntp2="195.83.132.135"

my_vm_grub_timeout=0
my_vm_grub_deb_extra_args="language=fr country=FR locale=fr_FR.UTF-8 time systemd.show_status=true apparmor=0 selinux=0 ipv6.disable=0 net.ifnames=1 biosdevname=1 ip=::::${my_vm_name}::on:::"
my_vm_grub_cos_extra_args="console=hvc0 apparmor=0 selinux=0 ipv6.disable=0 net.ifnames=0 biosdevname=1 lang=fr_FR.UTF-8 ip=dhcp"


### common tasks ###

echo "SSH config"
if [ -f /etc/ssh/sshd_config ] ; then
  sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/" /etc/ssh/sshd_config
  echo "AddressFamily inet" >> /etc/ssh/sshd_config
fi

### CentOS tasks ###

if [ -f /etc/centos-release ] ; then
  echo "User config"
  adduser -g 100 -G wheel ${my_vm_username}

  echo "SELinux config"
  sed -i "s/^SELINUX=.*$/SELINUX=disabled/"   /etc/sysconfig/selinux 

  echo "Grub config"
  sed -i "s/\(^GRUB_TIMEOUT=.*$\)/GRUB_TIMEOUT=${my_vm_grub_timeout}/"         /etc/default/grub
  sed -i "s/\(^GRUB_CMDLINE_LINUX=.*\)\"$/\1 ${my_vm_grub_cos_extra_args}\"/" /etc/default/grub
#  sed -i "s/\(^.*kernelopts=.*\)\"$/\1 ${my_vm_grub_cos_extra_args}\"/"       /boot/grub2/grub.cfg
  grub2-mkconfig -o /boot/grub2/grub.cfg 

  echo "Chrony/NTP config"
  sed -i "s/\(^OPTIONS=.*\)\"$/\1 -4\"/" /etc/sysconfig/chronyd
  sed -i "s/\(^pool.*$\)/#\1/" /etc/chrony.conf  
  /bin/echo "pool ${my_vm_ntp1} iburst" >> /etc/chrony.conf
  /bin/echo "pool ${my_vm_ntp2} iburst" >> /etc/chrony.conf

  echo "Package repos config"
  yum-config-manager --nogpgcheck --enable --save --setopt=baseos.baseurl=https://mirrors.ircam.fr/pub/CentOS/8-stream/BaseOS/x86_64/os
  yum-config-manager --nogpgcheck --enable --save --setopt=appstream.baseurl=https://mirrors.ircam.fr/pub/CentOS/8-stream/AppStream/x86_64/os
  yum-config-manager --nogpgcheck --enable --save --setopt=extras.baseurl=https://mirrors.ircam.fr/pub/CentOS/8-stream/extras/x86_64/os

  # echo "Network config"
  # nmcli connection modify System\ eth0 mtu ${my_vm_net_mtu}
  # nmcli connection modify "System ${my_vm_net_iface}" mtu ${my_vm_net_mtu}
  # nmcli connection up "System ${my_vm_net_iface}"

  echo "Locales and Timezone config"
  timedatectl set-timezone Europe/Paris

fi


### Debian tasks ###

if [ -f /etc/debian_version ] ; then
  echo "User config"
  adduser --gid 100 --gecos ${my_vm_username} --disabled-password ${my_vm_username}
  adduser ${my_vm_username} sudo

  echo "Grub config"
  sed -i "s/\(^GRUB_CMDLINE_LINUX=.*\)\"$/\1 ${my_vm_grub_deb_extra_args}\"/" /etc/default/grub
  sed -i "s/\(^GRUB_TIMEOUT=.*$\)/GRUB_TIMEOUT=${my_vm_grub_timeout}/" /etc/default/grub
  update-grub2

  # echo "Network config"
  # echo "
  # auto ${my_vm_net_iface}
  # iface ${my_vm_net_iface} inet dhcp
  #   post-up ip link set dev ${my_vm_net_iface} mtu ${my_vm_net_mtu}
  # " >> /etc/network/interfaces

  echo "Locales and Timezone config"
  echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8, fr_FR.UTF-8 UTF-8" | debconf-set-selections
  echo "locales locales/default_environment_locale select fr_FR.UTF-8"                            | debconf-set-selections
  sed -i 's/^# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen
  ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
  echo "tzdata tzdata/Areas select Europe"       | debconf-set-selections
  echo "tzdata tzdata/Zones/Europe select Paris" | debconf-set-selections

  dpkg-reconfigure --frontend=noninteractive locales
  dpkg-reconfigure --frontend=noninteractive tzdata

  echo "Chrony/NTP config"
  sed -i "s/\(^DAEMON_OPTS=.*\)\"$/\1 -4\"/" /etc/default/chrony 
  sed -i "s/\(^pool.*$\)/#\1/" /etc/chrony/chrony.conf  
  /bin/echo "pool ${my_vm_ntp1} iburst" >> /etc/chrony/chrony.conf
  /bin/echo "pool ${my_vm_ntp2} iburst" >> /etc/chrony/chrony.conf

  sed -i "s/^/#/" /etc/apt/sources.list
  
  my_deb_version=`cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2`

  /bin/echo "deb https://mirrors.ircam.fr/pub/debian/          ${my_deb_version}            main contrib non-free
deb https://mirrors.ircam.fr/pub/debian/          ${my_deb_version}-updates    main contrib non-free
deb https://mirrors.ircam.fr/pub/debian/          ${my_deb_version}-backports  main contrib non-free
deb https://mirrors.ircam.fr/pub/debian-security  ${my_deb_version}-security   main contrib non-free" > /etc/apt/sources.list.d/perso.list

fi

### Alpine tasks ###

if [ -f /etc/alpine-release ] ; then
  mount -o remount,rw /

  echo 'Enable Users'
  # chsh -s /bin/bash root
  adduser -h /home/${my_vm_username} -g 100 -G wheel -s /bin/bash -D ${my_vm_username}
  sed -i "s/^# \(%wheel ALL=(ALL) ALL$\)/\1/" /etc/sudoers

  echo 'Network configuration'
  cat > /etc/network/interfaces <<-EOFINTF
iface lo inet loopback
iface ${my_vm_net_iface} inet dhcp
 hostname ${my_vm_name}
 mtu ${my_vm_net_mtu}
EOFINTF
  ln -s networking /etc/init.d/net.lo
  rc-update add net.lo boot
  ln -s networking /etc/init.d/net.${my_vm_net_iface}
  rc-update add net.${my_vm_net_iface} default

  echo 'DNS configuration'
  setup-dns -d ${my_vm_domain} -n ${my_vm_net_dns}

  cat > /etc/resolv.conf <<-EOFRESOLVCONF
search ${my_vm_domain}
nameserver ${my_vm_net_dns}
EOFRESOLVCONF

  echo 'Timezone configuration'
  setup-timezone -z Europe/Paris

  echo 'Hostname configuration'
  setup-hostname ${my_vm_name}

  echo 'Keymap configuration'
  setup-keymap fr fr

  sed -Ei \
 -e 's/^[# ](rc_depend_strict)=.*/\1=NO/' \
 -e 's/^[# ](rc_logger)=.*/\1=YES/' \
 -e 's/^[# ](unicode)=.*/\1=YES/' \
 /etc/rc.conf

fi


### Fedora tasks ###

if [ -f /etc/fedora-release ] ; then
  echo "User config"
  adduser -g 100 -G wheel ${my_vm_username}

  echo "SELinux config"
  sed -i "s/^SELINUX=.*$/SELINUX=disabled/" /etc/sysconfig/selinux 
fi

exit 0

