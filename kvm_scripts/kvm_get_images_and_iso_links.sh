#!/bin/bash

### Alpine Linux ###

my_vm_alp_iso_path="http://mirrors.ircam.fr/pub/alpine/latest-stable/releases/x86_64/"
my_vm_alp_iso_file=$(wget -q -O - ${my_vm_alp_iso_path}| grep alpine-virt-[0-9.]*-x86_64.iso\" | cut -d\" -f8)
/bin/echo -e "\nAlpine DVD ISO file URL"
/bin/echo -e "${my_vm_alp_iso_path}${my_vm_alp_iso_file}"


### CentOS ###

my_vm_cos_bios_img_root="https://cloud.centos.org/centos/"
my_vm_cos_bios_img_vers=`wget -q -O - "${my_vm_cos_bios_img_root}?C=M;O=A" | grep odd | tail -n 1 | cut -d\" -f12`
my_vm_cos_bios_img_path="${my_vm_cos_bios_img_root}${my_vm_cos_bios_img_vers}x86_64/images/"
my_vm_cos_bios_img_file=$(wget -q -O - ${my_vm_cos_bios_img_path} | grep -o "CentOS-Stream-GenericCloud-${my_vm_cos_bios_img_vers/-stream\/}-[0-9]*.[0-9]*.x86_64.qcow2" | tac | head -n 1)
/bin/echo -e "\nCentOS BIOS QCOW2 IMG file URL"
/bin/echo -e "${my_vm_cos_bios_img_path}${my_vm_cos_bios_img_file}"

my_vm_cos_iso_root="http://mirrors.ircam.fr/pub/CentOS/"
my_vm_cos_iso_vers=$(wget -q -O - ${my_vm_cos_iso_root} | grep -e href..[0-9]-stream | tail -n 1 | cut -d\" -f8)
VM_COS_ORG_ISO_URL_SUFFIX="isos/x86_64/"
my_vm_cos_iso_file=$(wget -q -O - ${my_vm_cos_iso_root}${my_vm_cos_iso_vers}isos/x86_64/ | grep -o "CentOS-Stream-[0-9.]*-x86_64-latest-dvd1.iso" | tac | head -n 1)
/bin/echo -e "\nCentOS ISO file URL"
/bin/echo -e "${my_vm_cos_iso_root}${my_vm_cos_iso_vers}isos/x86_64/${my_vm_cos_iso_file}"

### Debian ###

my_vm_deb_uefi_img_root="https://cdimage.debian.org/cdimage/cloud/"
my_vm_deb_uefi_img_vers=`wget -q -O - "${my_vm_deb_uefi_img_root}?C=M;O=A" | sed "s/></>\n</g" | grep href | grep -v OpenStack | tail -n1 | cut -d\" -f2 | cut -d\/ -f1`
my_vm_deb_uefi_img_path="${my_vm_deb_uefi_img_root}/${my_vm_deb_uefi_img_vers}/latest/"
my_vm_deb_uefi_img_file=$(wget -q -O - ${my_vm_deb_uefi_img_path} | grep -o "debian-.*-genericcloud-amd64.qcow2" | cut -d\" -f1 | tac | head -n 1)
/bin/echo -e "\nDebian UEFI QCOW2 IMG file URL"
/bin/echo -e "${my_vm_deb_uefi_img_path}${my_vm_deb_uefi_img_file}"

my_vm_deb_net_iso_path="http://mirrors.ircam.fr/pub/debian-cd/current/amd64/iso-cd/"
my_vm_deb_net_iso_file=$(wget -q -O - ${my_vm_deb_net_iso_path} | grep -o "debian-[0-9.]*-amd64-netinst.iso" | tac | head -n 1)
/bin/echo -e "\nDebian Netboot ISO file URL"
/bin/echo -e "${my_vm_deb_net_iso_path}${my_vm_deb_net_iso_file}"

my_vm_deb_dvd_iso_path="http://mirrors.ircam.fr/pub/debian-cd/current/amd64/iso-dvd/"
my_vm_deb_dvd_iso_file=$(wget -q -O - ${my_vm_deb_dvd_iso_path} | grep -o "debian-[0-9.]*-amd64-DVD-[0-9]*.iso" | tac | head -n 1)
/bin/echo -e "\nDebian DVD ISO file URL"
/bin/echo -e "${my_vm_deb_dvd_iso_path}${my_vm_deb_dvd_iso_file}"

my_vm_deb_live_iso_path="http://mirrors.ircam.fr/pub/debian-cd/current-live/amd64/iso-hybrid/"
my_vm_deb_live_iso_file=$(wget -q -O - ${my_vm_deb_live_iso_path} | grep debian-live-[0-9.]*-amd64-standard.iso | cut -d\" -f8)
/bin/echo -e "\nDebian Live ISO file URL"
/bin/echo -e "${my_vm_deb_live_iso_path}${my_vm_deb_live_iso_file}"

### Fedora ###

my_vm_fed_bios_img_root="https://download.fedoraproject.org/pub/fedora/linux/releases/"
my_vm_fed_bios_img_vers=$(wget -q -O - ${my_vm_fed_bios_img_root} | grep href | tail -n 2 | head -n 1 | cut -d\" -f8)
my_vm_fed_bios_img_file=$(wget -q -O - ${my_vm_fed_bios_img_root}${my_vm_fed_bios_img_vers}Cloud/x86_64/images/ | grep -o "Fedora-Cloud-Base-[a-zA-Z0-9_.-]*.x86_64.qcow2" | tac | head -n 1)
/bin/echo -e "\nFedora BIOS QCOW2 IMG file URL"
/bin/echo -e "${my_vm_fed_bios_img_root}${my_vm_fed_bios_img_vers}Cloud/x86_64/images/${my_vm_fed_bios_img_file}"


### Ubuntu ###

my_vm_ubu_uefi_img_root="https://cloud-images.ubuntu.com/"
my_vm_ubu_uefi_img_vers=$(wget -q -O - ${my_vm_ubu_uefi_img_root} | grep folder.*href.. | tr -s \  | grep $(wget -q -O - ${my_vm_ubu_uefi_img_root} | grep folder.*href.. | tr -s \  | awk '{ print $13 }' | grep -e "^[0-9]" | sort | tail -n2 | head -n1 ) | cut -d\" -f10 | head -n1 )
my_vm_ubu_uefi_img_file=$(wget -q -O - ${my_vm_ubu_uefi_img_root}${my_vm_ubu_uefi_img_vers}current/ | grep "server-cloudimg-amd64-disk-kvm.img" | tac | head -n 1 | cut -d\" -f10)
/bin/echo -e "\nUbuntu UEFI QCOW2 IMG file URL"
/bin/echo -e "${my_vm_ubu_uefi_img_root}${my_vm_ubu_uefi_img_vers}current/${my_vm_ubu_uefi_img_file}"

exit 0


