
# Download and run script
```bash
bash <(wget -qO- https://bitbucket.org/poubl31/publicrepo/raw/HEAD/get_files.sh)
```
Or
```bash
bash <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/get_files.sh)
```

# Create/Delete KVM microvm 
```bash
bash -c 'my_vm_name="mytestvm2" my_vm_username="admin2" my_vm_network="brkvm" source <(curl -s https://bitbucket.org/poubl31/publicrepo/raw/HEAD/kvm_scripts/kvm_create_deb_mvm_direct.sh)'

bash -c 'my_vm_name="mytestvm2" ; virsh destroy ${my_vm_name} ; virsh undefine ${my_vm_name} --nvram --remove-all-storage'
```

# Usefull session commands
```bash
killall ssh-agent ; \
eval $(ssh-agent -s) ; \
ssh-add ; \
tmux attach-session
```

# Links
[https://bitbucket.org/poubl31/publicrepo/raw/HEAD/get_files.sh](https://bitbucket.org/poubl31/publicrepo/raw/HEAD/get_files.sh)
